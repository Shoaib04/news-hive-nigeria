package com.enigmaproapps.newshivenigeria;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.util.Log;
import android.util.LruCache;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import neededObjects.DigitsUpdateEvent;
import neededObjects.EventfeedLoaded;
import neededObjects.NewsItem;
import neededObjects.NewsSource;
import wokerClasses.RssFeedLoaderService;
import wokerClasses.alarmReceiver;
import wokerClasses.fileStorageHandler;

/**
 * Created by Shoaib on 5/28/2016.
 */
public class globalClass extends Application implements appConstants {

    private static ArrayList<NewsSource> newsSourcesList = null;
    public static NewsSource NewsSource_AllUnreads = null;
    public static NewsSource NewsSource_MyFavorites = null;

    private static fileStorageHandler storageHandler;
    private static RequestQueue mRequestQueue = null;
    private static ImageLoader imageLoader = null;

    private static globalClass sInstance = null;
    private final String category_Source_News = "news";
    private static boolean feedAlreadyStored = false;
    public static boolean NetworkStatus;
    public static boolean toLoad = true;
    public static boolean canMainCallFillMenu = false;

    // Restart service every *** seconds/Minutes
    private static long syncIntervalTime = appConstants.AlarmManger_INTERVAL_HALFHOUR;

    private static String App_CurrentThemeName = "AppThemeNaija";

    SharedPreferences mPrefs;
    SharedPreferences.Editor mPrefsEditor;
    public static String MainActivityStatePreserveBundle_SourceName;
    public static int MainActivityStatePreserveBundle_ScrollPosition;
    public static ArrayList<NewsItem> MainActivityStatePreserveBundle_NewsItemsListToMaintain;

    public static boolean isAppRunning_HasMainActivityChangedMe = false;
    public static int currentList_Style = R.layout.news_list_item_ascard;


    private NewsSource MainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        storageHandler = new fileStorageHandler(this);
        checkNetworkState();

        AppPrefsManager theAppPrefsMaintainer = AppPrefsManager.getInstance();
        theAppPrefsMaintainer.setGlobalContext(this);

        mPrefs = getSharedPreferences(App_SelectedTheme_SharedPrefsFilename, MODE_PRIVATE);
        mPrefsEditor = mPrefs.edit();

        if (mRequestQueue == null) {
            mRequestQueue = makePlusStartRequestQueue();
        }

        if (imageLoader == null) {
            imageLoader = new ImageLoader(mRequestQueue,
                    new ImageLoader.ImageCache() {
                        private final LruCache<String, Bitmap>
                                cache = new LruCache<String, Bitmap>(20);

                        @Override
                        public Bitmap getBitmap(String url) {
                            return cache.get(url);
                        }

                        @Override
                        public void putBitmap(String url, Bitmap bitmap) {
                            cache.put(url, bitmap);
                        }
                    });
        }
        registerToAlarmManager(null);
    }

    @Override
    public void COUNTRY_FILENAME_MAP_FILLER() {

    }

    public void setSyncIntervalTime (long givenTime)
    {
        syncIntervalTime = givenTime;
        AppPrefsManager.getInstance().setSyncIntervalTimeInPrefs(syncIntervalTime);
    }

    public void registerToAlarmManager(String callerName) {

        syncIntervalTime = AppPrefsManager.getInstance().getSyncInterval();

        Intent intent = new Intent(this, alarmReceiver.class);
        boolean alarmRunning = (PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_NO_CREATE) != null);

        if (alarmRunning == false || callerName!=null) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime()+syncIntervalTime,
                    syncIntervalTime, pendingIntent);

        }
    }

    public static globalClass getInstance() {
        return sInstance;
    }

    public void MakeToast() {

        Toast.makeText(this, "Alarm aya he", Toast.LENGTH_SHORT).show();


    }

    private boolean checkNetworkState() {

        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    public String getApp_CurrentThemeName() {
        return mPrefs.getString(appConstants.App_SelectedTheme_SharedPrefsKey, "AppThemeNaija");
    }

    public void setApp_CurrentThemeName(String app_CurrentThemeName) {
        App_CurrentThemeName = app_CurrentThemeName;
        mPrefsEditor.putString(appConstants.App_SelectedTheme_SharedPrefsKey, App_CurrentThemeName);
        mPrefsEditor.commit();
    }

    public void setMainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow(NewsSource Ns)
    {
        this.MainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow = Ns;
    }

    public NewsSource getMainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow()
    {
        return this.MainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow;
    }

    public void giveDigitsAccordingTofeedsInStorageToNavMenu(Menu navMenu) {
        //let the mainActivity's navDrawer Menu Items show the number of news they have now
        //by the way these news are old ones restored from memory
        TextView mTextView;
        for (int i = 0; i < newsSourcesList.size(); i++) {
            if (newsSourcesList.get(i).toShow) {
                for (int j = 0; j < navMenu.size(); j++) {
                    if (newsSourcesList.get(i).sourcename.equals(navMenu.getItem(j).getTitle())) {
                        mTextView = (TextView) navMenu.getItem(j).getActionView().findViewById(R.id.actionViewText);
                        int no = getNewsSourceByName(newsSourcesList.get(i).sourcename).getUnreadNews_Number();
                        mTextView.setText(Integer.toString(no));
                        break;
                    }
                }
            }

        }
    }

    private NewsSource giveNewsSouceObjectFromName(String name) {
        for (int i = 0; i < newsSourcesList.size(); i++) {
            if (newsSourcesList.get(i).toShow) {
                if (newsSourcesList.get(i).sourcename.equals(name)) {
                    return newsSourcesList.get(i);
                }
            }
        }

        return null;
    }

    private final ResultReceiver resultReceiver = new ResultReceiver(new Handler()) {
        @SuppressWarnings("unchecked")
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            NewsSource mGottenSource = (NewsSource) resultData.getSerializable(appConstants.IntentParam_serviceSendingResultingNewsSourceObject);
            adjustThisNewsSourceInTheMainList(mGottenSource);
            //Have commented this because i had forgotten what this was for
            //EventBus.getDefault().post(new EventfeedLoaded(mGottenSource.sourcename));
            EventBus.getDefault().post(new DigitsUpdateEvent(mGottenSource.sourcename, "updateDigits"));
        }

    };

    private void adjustThisNewsSourceInTheMainList(NewsSource thesource) {
        int index = -1;
        for (int i = 0; i < newsSourcesList.size(); i++) {
            if (thesource.sourcename.equals(newsSourcesList.get(i).sourcename)) {
                index = i;
                newsSourcesList.remove(index);
                newsSourcesList.add(index, thesource);
            }
        }
    }

    public void mainRegisteredEvent() {
        //Check if there are previously stored NewsItems in Memory. if Yes than populate the HashMap with them...
        //And whether yes or not... Check network status and reload the latest Feed into HashMap
        LoadSelectedNewsSoucesShowInMainActivity();
    }

    public void RefreshASingleSource(String newsSourceName) {


        if (getNewsSourceByName(newsSourceName) != null) {
            retriveOnlineFeedForMe(getNewsSourceByName(newsSourceName));
        }

    }

    public void LoadSelectedNewsSoucesShowInMainActivity() {

        boolean caseWhen_OpeningAppRequiresNetworkWork = false;

        //Restore previous NewsData for all NewsSources (if exists) otherwise just create a new instance
        if (newsSourcesList == null) {
            newsSourcesList = (ArrayList<NewsSource>) storageHandler.readObject(ArrayList.class, appConstants.File_allSourcesFeedHashMap);
            if (newsSourcesList == null) {
                //Shows there was no previously stored feeds, so we have to load them
                fillSources();
                caseWhen_OpeningAppRequiresNetworkWork = true;
            } else if (newsSourcesList.size() <= 1) {
                //Shows there was no previously stored feeds, so we have to load them
                fillSources();
                caseWhen_OpeningAppRequiresNetworkWork = true;
            }
        }

        //Tell the MainActivity to Fill the Menu Now Since we Have populated the NewsSourcesList.
        EventBus.getDefault().post(new EventfeedLoaded("", "fillMenu"));

        if (checkNetworkState() && caseWhen_OpeningAppRequiresNetworkWork) {
            //Things to do when Internet Connection is Available.....
            NetworkStatus = true;

            //Load Latest feeds for all News Sources
            for (int i = 0; i < newsSourcesList.size(); i++) {
                retriveOnlineFeedForMe(newsSourcesList.get(i));
            }

        }
        else if (!checkNetworkState()) {
            NetworkStatus = false;
            String noConnection = "noConnection";
            EventBus.getDefault().post(new EventfeedLoaded(noConnection));
        }

        if (NewsSource_AllUnreads == null || NewsSource_MyFavorites==null)
        {
            NewsSource_MyFavorites = new NewsSource();
            NewsSource_MyFavorites.NewItemsGotten =0;
            NewsSource_MyFavorites.sourcename = appConstants.navMenu_Catergory_Favourites;
            NewsSource_MyFavorites.newsItemArrayList = new ArrayList<>();

            NewsSource_AllUnreads = new NewsSource();
            NewsSource_AllUnreads.NewItemsGotten =0;
            NewsSource_AllUnreads.sourcename = appConstants.navMenu_Catergory_AllUnreads;
            NewsSource_AllUnreads.newsItemArrayList = new ArrayList<>();
        }
    }

    public void makeCallToRetriveOnlineFeedForMe ()
    {
        if (checkNetworkState()) {
            //Things to do when Internet Connection is Available.....
            NetworkStatus = true;

            //Load Latest feeds for all News Sources
            for (int i = 0; i < newsSourcesList.size(); i++) {
                retriveOnlineFeedForMe(newsSourcesList.get(i));
            }

        }
        else if (!checkNetworkState()) {
            NetworkStatus = false;
            String noConnection = "noConnection";
            EventBus.getDefault().post(new EventfeedLoaded(noConnection));
        }

    }

    public void retriveOnlineFeedForMe(NewsSource source) {
        if (source.toShow) {

            Intent intent = new Intent(globalClass.this, RssFeedLoaderService.class);
            intent.putExtra(appConstants.IntentParam_globalToRssService_NewsSourceObject, source);
            intent.putExtra(appConstants.Service_globalClass_ResultReceiverName, resultReceiver);
            startService(intent);
        }

    }

    public NewsSource getNewsSourceByName(String SourceName) {

        if (SourceName==null)
        {
            return null;
        }

        if (SourceName.equals(appConstants.navMenu_Catergory_AllUnreads) || SourceName.equals(appConstants.navMenu_Catergory_Favourites))
        {
            if (SourceName.equals(appConstants.navMenu_Catergory_AllUnreads))
            {
                return NewsSource_AllUnreads;
            }
            else
            {
                return NewsSource_MyFavorites;
            }
        }
        if (SourceName != null) {
            for (int i = 0; i < newsSourcesList.size(); i++) {
                if (newsSourcesList.get(i).sourcename.equals(SourceName)) {
                    return newsSourcesList.get(i);
                }
            }
        }

        return null;
    }

    public RequestQueue getmRequestQueue() {

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {

        return imageLoader;
    }

    public RequestQueue makePlusStartRequestQueue() {
        if (mRequestQueue == null) {
            Cache cache = new DiskBasedCache(this.getCacheDir(), 10 * 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            // Don't forget to start the volley request queue
            mRequestQueue.start();
        }
        return mRequestQueue;
    }

    public void fillSources() {
        newsSourcesList = new ArrayList<>();
        for (int i = 0; i < newsSourceNamesArray.length; i++) {
            newsSourcesList.add(new NewsSource(newsSourceNamesArray[i], newsSourceLinks[i], category_Source_News, true));

        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        maintainPreviousState();
    }

    public void maintainPreviousState() {
        if (newsSourcesList != null) {
            storageHandler.writeObject(appConstants.File_allSourcesFeedHashMap, newsSourcesList);
            Log.e("", "");
        }
//        if (!feedAlreadyStored && newsSourcesList != null) {
//            storageHandler.writeObject(appConstants.File_allSourcesFeedHashMap, newsSourcesList);
//        }

    }

    public ArrayList<NewsSource> getNewsSourcesList() {

        return newsSourcesList;
    }

    //Methods Start here to catter give and take of fields that maintain
    //State of MainActivity .........
    public void setNewsArrayList_WhichIs_Maintained(ArrayList<NewsItem> gottenList) {
        if (MainActivityStatePreserveBundle_NewsItemsListToMaintain != null) {
            MainActivityStatePreserveBundle_NewsItemsListToMaintain.clear();
            MainActivityStatePreserveBundle_NewsItemsListToMaintain.addAll(gottenList);
            return;
        } else if (MainActivityStatePreserveBundle_NewsItemsListToMaintain == null) {
            MainActivityStatePreserveBundle_NewsItemsListToMaintain = new ArrayList<>();
            MainActivityStatePreserveBundle_NewsItemsListToMaintain.addAll(gottenList);
        }

    }

    public ArrayList<NewsItem> getNewsArrayList_WhichIs_Maintained() {
        return MainActivityStatePreserveBundle_NewsItemsListToMaintain;
    }
}
