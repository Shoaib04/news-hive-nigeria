package com.enigmaproapps.newshivenigeria;

/**
 * Created by Shoaib on 6/1/2016.
 */
public interface appConstants {

    interface LongClickListener{

        void onItemSelectedFromContextMenu(int position);
    }


    void COUNTRY_FILENAME_MAP_FILLER();
    int EXTRAHIGH_RESOLUTION_DENTSITY_VAL = 4;
    int HIGH_RESOLUTION_DENTSITY_VAL = 3;
    int MEDIUM_RESOLUTION_DENTSITY_VAL = 2;
    int SMALL_RESOLUTION_DENTSITY_VAL = 2;

    String CONTACT_EMAIL_ADDRESS = "enigma_apps@hotmail.com";
    String FeedBack_EMAIL_SUBJECT = "NEWS HIVE - Nigeria Feedback Report";
    String AdMob_AppID = "ca-app-pub-0289787001943877~9698557688";

    String newsSourceNamesArray[] = {"Information Nigeria", "National Mirror", "ThisDayLive", "FootballLive",
            "Channels", "Naij", "Punch", "AbokiFX", "PM News", "Sun News", "Daily Post", "Leadership", "Talk of Naija", "The Guardian",
            "The Trent Online", "Premium Times", "Vanguard News", "The Nation", "Nigerian Eye"};

    //"Latest Movies (NollyWood)" to be added
    //naij=entry instead of item
    String newsSourceLinks[] = {"http://www.informationng.com/rss", "http://nationalmirroronline.net/new/rss",
            "http://www.thisdaylive.com/index.php/comments/feed/", "http://footballlive.ng/footballnews/feed/",
            "http://www.channelstv.com/feed/", "https://www.naij.com/rss/all.rss", "http://www.punchng.com/feed/", "http://abokifx.com/feed",
            "http://www.pmnewsnigeria.com/feed/", "http://sunnewsonline.com/feed/", "http://dailypost.ng/feed/", "http://leadership.ng/feed",
            "http://talkofnaija.com/feed/", "http://guardian.ng/feed/", "http://www.thetrentonline.com/feed/", "http://www.premiumtimesng.com/feed",
            "http://www.vanguardngr.com/feed/", "http://thenationonlineng.net/feed/", "http://www.nigerianeye.com/feeds/posts/default?alt=rss"};

    String App_SelectedTheme_SharedPrefsFilename = "appsThemeToSet_Filename";
    String App_SelectedTheme_SharedPrefsKey = "appsThemeToSet";

    String navMenu_Catergory_Favourites = "My Favourites";
    String navMenu_Catergory_AllUnreads = "All Unreads";


    String MainActivity_ListSort_Mode_RecentToOld="rtoO";
    String MainActivity_ListSort_Mode_OldToRecent="otoR";


    String StatePreserveKey_SourceName_MainActivity = "statePreserveSourceName";
    String StatePreserveKey_ScrollPosition_MainActivity = "statePreserveSourceName";

    String File_allSourcesFeedHashMap = "nigeriaNewsFeedFile.ser";
    String File_newsSoucesSelectsAndUnselected = "nigeriaNewsSourcesSelectionFile.ser";


    String IntentParam_serviceSendingResultingNewsSourceObject = "rssResultFeedList";
    String Service_globalClass_ResultReceiverName = "serviceGlobalReceiver";
    String IntentParam_ListIndex_MaintoDetailActivity = "listIndex";
    String IntentParam_globalToRssService_NewsSourceObject = "NewsObjectGlobalToRSSService";
    String IntentParam_NewsDetailActivity_To_DetailActivityWithWebView = "paramtoNextActivity";
    String IntentParam_NewsDetailActivity_To_DetailActivityWithWebView1 = "param0";
    String IntentParam_AboutActivity_recognizesActivity = "paRam1";

    String IMAGEVIEWER_PHOTOSAVING_DIRECTORYNAME = "NewsNigeria_Images";
    String IntentParam_NewsDetailActivity_To_ImageViewerActivity = "thenewstoimageviewerintentparam";
    String IntentParam_MainToDetialActivitySourceName = "theSourceVerf";

    long AlarmManger_INTERVAL_1MINUTE = 60 * 1000;
    long AlarmManger_INTERVAL_2MINUTE = 120 * 1000;
    long AlarmManger_INTERVAL_10MINUTES = 600 * 1000;
    long AlarmManger_INTERVAL_15MINUTES = 900 * 1000;
    long AlarmManger_INTERVAL_HALFHOUR = 1800 * 1000;
    long AlarmManger_INTERVAL_HOUR = 3600 * 1000;
    long AlarmManger_INTERVAL_2HOUR = 7200 * 1000;
    long AlarmManger_INTERVAL_4HOUR = 14400 * 1000;
    long AlarmManger_INTERVAL_DAY = 86400 * 1000;

}
