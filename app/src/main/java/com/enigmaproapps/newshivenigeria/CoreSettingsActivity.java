package com.enigmaproapps.newshivenigeria;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;


import java.util.List;


public class CoreSettingsActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        super.onBuildHeaders(target);

        loadHeadersFromResource(R.xml.pref_headers,target);

    }

    @Override
    public void onHeaderClick(Header header, int position) {
        super.onHeaderClick(header, position);

        if (header.id==R.id.prefHeader_About)
        {
            Intent intent = new Intent(CoreSettingsActivity.this,AboutActivity.class);
            intent.putExtra(appConstants.IntentParam_AboutActivity_recognizesActivity,CoreSettingsActivity.class.getName());
            startActivity(intent);
        }
        else if (header.id== R.id.prefHeader_Feedback)
        {
            String deviceInfo = "OS version: " + System.getProperty("os.version")
                    + " Device: " + android.os.Build.DEVICE
                    + " Model: " + android.os.Build.MODEL
                    + " Product: " + android.os.Build.PRODUCT;

            Intent send = new Intent(Intent.ACTION_SENDTO);
            String uriText = "mailto:" + Uri.encode(appConstants.CONTACT_EMAIL_ADDRESS) +
                    "?subject=" + Uri.encode(appConstants.FeedBack_EMAIL_SUBJECT) +
                    "&body=" + Uri.encode("Device Information-- " + deviceInfo);
            Uri uri = Uri.parse(uriText);

            send.setData(uri);
            startActivity(Intent.createChooser(send, "Send mail..."));
        }
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        if (SynchronizationFragment_Pref.class.getName().equals(fragmentName))
        {
            return true;
        }
        else if (MessagesFragment_Pref.class.getName().equals(fragmentName))
        {
            return true;
        }
        else if (NotificationsFragment_Pref.class.getName().equals(fragmentName))
        {
            return true;
        }

        return false;
    }

    //Fragment for Synchronization
    public static class SynchronizationFragment_Pref extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.pref_data_sync);

            ListPreference preference = (ListPreference) this.findPreference(getResources().getString(R.string.Prefs_Synchronization_backSyncInterval_Key));
            //In the list set the index that is selected currently and saved, we get it from the AppPrefsManager
            preference.setValueIndex(AppPrefsManager.getInstance().getDefaultSyncListValueIndex());
            preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    long time = AppPrefsManager.getInstance().getSyncInterval((String)newValue);
                    ((globalClass)getActivity().getApplication()).setSyncIntervalTime(AppPrefsManager.getInstance().getSyncInterval((String)newValue));
                    AppPrefsManager.getInstance().setSyncIntervalTimeInPrefs(AppPrefsManager.getInstance().getSyncInterval((String)newValue));
                    ((globalClass)getActivity().getApplication()).registerToAlarmManager(CoreSettingsActivity.class.getName());
                    return true;
                }
            });
        }
    }

    //Fragment for Messages
    public static class MessagesFragment_Pref extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.pref_messages_store);

            ListPreference preference = (ListPreference) this.findPreference(getResources().getString(R.string.Prefs_Messages_autoCleanRead_keyAlt2));
            //In the list set the index that is selected currently and saved, we get it from the AppPrefsManager
            preference.setValueIndex(AppPrefsManager.getInstance().getDefault_AutoCleanUpRead_ListValueIndex());

            ListPreference preferenceUnreadList = (ListPreference) this.findPreference(getResources().getString(R.string.Prefs_Messages_autoCleanUnread_key));
            //In the list set the index that is selected currently and saved, we get it from the AppPrefsManager
            preferenceUnreadList.setValueIndex(AppPrefsManager.getInstance().getDefault_AutoCleanUpUnredRead_ListValueIndex_Unread());
        }
    }

    //Fragment for Messages
    public static class NotificationsFragment_Pref extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.pref_notifications);
        }
    }
}
