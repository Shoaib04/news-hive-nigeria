package com.enigmaproapps.newshivenigeria;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by Shoaib on 6/22/2016.
 */

public class AppPrefsManager {

    private static Context globalContext = null;
    private static AppPrefsManager sInstance = null;
    private static SharedPreferences appPrefs;
    SharedPreferences.Editor mEditor;


    private AppPrefsManager() {

    }

    public synchronized static AppPrefsManager getInstance() {
        //Method synchronized since in multithreaded scenerio multiple
        //Instances might be allocated.
            if (sInstance == null) {
                sInstance = new AppPrefsManager();
            }
            return sInstance;
    }

    public void setGlobalContext(Context givenContext) {
        globalContext = givenContext;
        appPrefs = PreferenceManager.getDefaultSharedPreferences(globalContext);
        mEditor = appPrefs.edit();
    }

    public boolean hasContextAlready() {
        if (globalContext != null) {
            return true;
        }
        return false;
    }

    public boolean isBackgroundSyncEnabled ()
    {
        String getValuesName = globalContext.getResources().getString(R.string.Prefs_Synchronization_autoBackSyncEnabledOrDisabled_Key);
        return appPrefs.getBoolean(getValuesName,true);
    }

    public long getSyncInterval ()
    {
        String getValuesName = globalContext.getResources().getString(R.string.Prefs_Synchronization_backSyncInterval_Key);
        long ppTime = 30*60*1000;
        String ppTimeString;

        ppTimeString=String.valueOf(ppTime);

        String timeGottenFromPrefs = appPrefs.getString(getValuesName,ppTimeString);
        timeGottenFromPrefs = String.valueOf(getSyncInterval(timeGottenFromPrefs));
        Log.d("","");
        return Long.parseLong(timeGottenFromPrefs);
    }

    public int getDefaultSyncListValueIndex ()
    {
        String interValString = appPrefs.getString(globalContext.getResources().getString(R.string.Prefs_Synchronization_backSyncInterval_Key),"halfHour");
        String[] arr = globalContext.getResources().getStringArray(R.array.syncFrequencyValues);
        int indexVal=arr.length -1;

        for (int i = 0; i < arr.length; i++) {

            if (interValString.equals(arr[i]))
            {
                indexVal = i;
                break;
            }
        }
        if (indexVal>=arr.length)
        {
            indexVal = arr.length -1;
        }
        return indexVal;
    }


    public int getDefault_AutoCleanUpUnredRead_ListValueIndex_Unread()
    {
        String interValString = appPrefs.getString(globalContext.getResources().getString(R.string.Prefs_Messages_autoCleanUnread_key),"oneDay");
        String[] arr = globalContext.getResources().getStringArray(R.array.Prefs_messagesstore_autoclean_entries);
        String[] ValArr = globalContext.getResources().getStringArray(R.array.Prefs_messagesstore_autoclean_entryValues);
        int indexVal=ValArr.length -1;

        for (int i = 0; i < arr.length; i++) {

            if (interValString.equals(arr[i]))
            {
                indexVal = i;
                break;
            }
        }
        if (indexVal>=arr.length)
        {
            indexVal = arr.length -1;
        }
        return indexVal;
    }

    public int getDefault_AutoCleanUpRead_ListValueIndex ()
    {
        String interValString = appPrefs.getString(globalContext.getResources().getString(R.string.Prefs_Messages_autoCleanRead_keyAlt2),"oneDay");
        String[] arr = globalContext.getResources().getStringArray(R.array.Prefs_messagesstore_autoclean_entries);
        String[] ValArr = globalContext.getResources().getStringArray(R.array.Prefs_messagesstore_autoclean_entryValues);
        int indexVal=ValArr.length -1;

        for (int i = 0; i < arr.length; i++) {

            if (interValString.equals(arr[i]))
            {
                indexVal = i;
                break;
            }
        }
        if (indexVal>=arr.length)
        {
            indexVal = arr.length -1;
        }
        return indexVal;
    }

    public void setSyncIntervalTimeInPrefs (long time)
    {
        mEditor.putString(globalContext.getResources().getString(R.string.Prefs_Synchronization_backSyncInterval_Key),String.valueOf(time));
        mEditor.commit();
    }

    public long getSyncInterval (String gottenValue)
    {
        if (gottenValue.equals("oneDay"))
        {
            return appConstants.AlarmManger_INTERVAL_DAY;
        }
        else if (gottenValue.equals("oneHour"))
        {
            return appConstants.AlarmManger_INTERVAL_HOUR;
        }
        else if (gottenValue.equals("halfHour"))
        {
            return appConstants.AlarmManger_INTERVAL_HALFHOUR;
        }
        else if (gottenValue.equals("fifMinutes"))
        {
            return appConstants.AlarmManger_INTERVAL_15MINUTES;
        }
        else if (gottenValue.equals("twoMinutes"))
        {
            return appConstants.AlarmManger_INTERVAL_2MINUTE;
        }
        else if (gottenValue.equals("oneMinutes"))
        {
            return appConstants.AlarmManger_INTERVAL_1MINUTE;
        }
        else
        {
            if (gottenValue.contains("0"))
            {
                return Long.valueOf(gottenValue);
            }
        }

        return appConstants.AlarmManger_INTERVAL_HALFHOUR;
    }

    public long getWhenRemoveReadItems ()
    {
        String getValuesName = globalContext.getResources().getString(R.string.Prefs_Messages_autoCleanRead_keyAlt2);

        long ppTime = 24*60*60*1000;
        String ppTimeString;
        ppTimeString=String.valueOf(ppTime);

        String intervalForRemoval = appPrefs.getString(getValuesName,ppTimeString);
        if (intervalForRemoval.equals("Never"))
        {
            return 0;
        }
        else
        {
            return getSyncInterval(intervalForRemoval);
        }
    }

    public boolean getIfNotsForAutoSycnEnabled ()
    {
        return appPrefs.getBoolean(globalContext.getResources().getString(R.string.Prefs_Notification_EnableDisable_Key),true);
    }
    public boolean getIfVibrationForAutoSycnEnabled ()
    {
        return appPrefs.getBoolean(globalContext.getResources().getString(R.string.Prefs_Notification_VibrateOrNot_Key),true);
    }

}
