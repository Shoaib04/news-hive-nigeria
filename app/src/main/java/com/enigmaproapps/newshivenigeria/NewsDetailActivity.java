package com.enigmaproapps.newshivenigeria;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

import neededObjects.NewsItem;
import neededObjects.NewsSource;

public class NewsDetailActivity extends AppCompatActivity {

    WebView webView;
    String webViewLink;

    NewsSource CurrentNewsSource;
    int currentIndexForNewsItem;
    String HeadingText;
    String NewsDetailText;
    String TemplateHtml;

    boolean toLauchNewActivity = false;

    private String ImageStyling = "img{max-width:300; height: auto;}";
    private String StylingConstant = "<style> video{max-width:300; height: auto;}" + ImageStyling + "</style>";
    private String StylingVar = StylingConstant;
    private static final String toBeReplaced = "+0000";
    private static final String toPutForReplacement = "";


    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        setCorrectTheme();
        setContentView(R.layout.activity_news_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.selectionToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.newsDetailWebView);
        //Get the Index of the NewsItem Selected from the List in Previous Activity
        currentIndexForNewsItem = getIntent().getIntExtra(appConstants.IntentParam_ListIndex_MaintoDetailActivity, 0);
        //Get the NewsSource that was being browsed in Previous activity by following globalClass Method
        CurrentNewsSource = ((globalClass) getApplication()).getMainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow();
        //Bundle b = getIntent().getBundleExtra(appConstants.IntentParam_BundleKey_MainToDetialActivity);

        alotAllContentForThisPage();
        LoadPlusShowBannerAd();

//        webView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return (event.getAction() == MotionEvent.ACTION_MOVE);
//            }
//        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);

//        <meta name="viewport" content="width=device-width, initial-scale=1">
        webView.loadDataWithBaseURL(null, "<html><head>" +
                " <script>  var video = document.getElementsByTagName(\"video\")[0]; video.setAttribute('width', '300'); video.width = 700;  </script> " + StylingVar + "</head>" +
                " <body> <div style=\"max-width:100%\";>" + TemplateHtml + webViewLink + "</div></body>" +
                "</html>", "text/html", "charset=utf-8", null);

//        webView.setClickable(true);
//        webView.setEnabled(true);
//        webView.setFocusable(true);
//        webView.setFocusableInTouchMode(true);
        webView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebView.HitTestResult hitTestResult = ((WebView) v).getHitTestResult();
                int valueTYpe = hitTestResult.getType();
                String aho = hitTestResult.getExtra();

                if (hitTestResult.getType() == WebView.HitTestResult.IMAGE_TYPE
                        || hitTestResult.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE
                        || hitTestResult.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE) {

                    if (true) {
                        toLauchNewActivity = true;
                        if (hitTestResult.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE) {
                            Intent intent = new Intent(NewsDetailActivity.this, DetailActivityWithWebView.class);
                            intent.putExtra(appConstants.IntentParam_NewsDetailActivity_To_DetailActivityWithWebView1, CurrentNewsSource.getNewsItemArrayList().get(currentIndexForNewsItem).title);
                            intent.putExtra(appConstants.IntentParam_NewsDetailActivity_To_DetailActivityWithWebView, hitTestResult.getExtra());
                            startActivity(intent);
                            Toast.makeText(NewsDetailActivity.this, hitTestResult.getExtra(), Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(NewsDetailActivity.this, ImageViewerActivity.class);
                            intent.putExtra(appConstants.IntentParam_NewsDetailActivity_To_ImageViewerActivity, hitTestResult.getExtra());
                            startActivity(intent);
                            Toast.makeText(NewsDetailActivity.this, hitTestResult.getExtra(), Toast.LENGTH_SHORT).show();
                        }
                        //return true;
                    } else {

                        toLauchNewActivity = false;
                    }
                }

            }
        });
//        webView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                WebView.HitTestResult hitTestResult = ((WebView) v).getHitTestResult();
//                int valueTYpe = hitTestResult.getType();
//                String aho = hitTestResult.getExtra();
//
//                if (event.getAction() == MotionEvent.ACTION_DOWN && (hitTestResult.getType() == WebView.HitTestResult.IMAGE_TYPE
//                        || hitTestResult.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE
//                        || hitTestResult.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE)) {
//
//                    if (true) {
//                        toLauchNewActivity = true;
//                        if (hitTestResult.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE) {
//                            Intent intent = new Intent(NewsDetailActivity.this, DetailActivityWithWebView.class);
//                            intent.putExtra(appConstants.IntentParam_NewsDetailActivity_To_DetailActivityWithWebView1, CurrentNewsSource.getNewsItemArrayList().get(currentIndexForNewsItem).title);
//                            intent.putExtra(appConstants.IntentParam_NewsDetailActivity_To_DetailActivityWithWebView, hitTestResult.getExtra());
//                            startActivity(intent);
//                            Toast.makeText(NewsDetailActivity.this, hitTestResult.getExtra(), Toast.LENGTH_SHORT).show();
//                        } else {
//                            Intent intent = new Intent(NewsDetailActivity.this, ImageViewerActivity.class);
//                            intent.putExtra(appConstants.IntentParam_NewsDetailActivity_To_ImageViewerActivity, hitTestResult.getExtra());
//                            startActivity(intent);
//                            Toast.makeText(NewsDetailActivity.this, hitTestResult.getExtra(), Toast.LENGTH_SHORT).show();
//                        }
//                        return true;
//                    } else {
//
//                        toLauchNewActivity = false;
//                    }
//                }
//
//
//                return false;
//            }
//        });

        fab = (FloatingActionButton) findViewById(R.id.newsDetailFloatingButton);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = CurrentNewsSource.getNewsItemArrayList().get(currentIndexForNewsItem).title + ".  " + CurrentNewsSource.getNewsItemArrayList().get(currentIndexForNewsItem).link;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, CurrentNewsSource.getNewsItemArrayList().get(currentIndexForNewsItem).title);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

    }

    private void LoadPlusShowBannerAd()
    {
        AdView mAdView = (AdView) findViewById(R.id.adView_NewsDetailActivity);
        AdRequest adRequest = new AdRequest.Builder().build();
//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice()// All emulators
//                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(NewsDetailActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


        super.onBackPressed();
    }


    private void alotAllContentForThisPage() {

        int widthDIPS = 140;
        final float DIP_CONSTANT_STANDARD = 160.0f;
        //DisplayMetrics metrics = getResources().getDisplayMetrics();

        //float ratio =   metrics.densityDpi / DIP_CONSTANT_STANDARD;
//        if (ratio <= appConstants.EXTRAHIGH_RESOLUTION_DENTSITY_VAL)
//        {
//            widthDIPS = 85;
//        }
//        if (ratio <= appConstants.HIGH_RESOLUTION_DENTSITY_VAL)
//        {
//            widthDIPS = 100;
//        }
//        if ( ratio<= appConstants.MEDIUM_RESOLUTION_DENTSITY_VAL)
//        {
//            widthDIPS = 170;
//        }
        //float width =  (widthDIPS * ratio + 0.5f);

        int width = giveDpToPx(widthDIPS);
        String px = "";
        ImageStyling = " text{text-align: center;} img{max-width:" + String.valueOf(width) + px + "; height: auto; clear:both; display: block;" +
                " margin-left: auto; margin-right: auto;} ";
        String VideoStyling = " video{max-width:" + String.valueOf(width) + px + "; height: auto;} ";

        StylingConstant = "<style>" + VideoStyling + ImageStyling + "div{max-width:100%; height: auto;}</style>";
        StylingVar = "<style>" + VideoStyling + ImageStyling + "div{max-width:100%; height: auto;}</style>";

        //Alot correct Styling and values to WebView Content Here
        String name = ((globalClass) getApplication()).getApp_CurrentThemeName();
        if (name.equals("AppThemeNaija")) {
            StylingVar = "<style>" + VideoStyling + ImageStyling + "body{color: white; background: #262626;} div{max-width:100%; height: auto;}</style>";
        } else if (name.equals("AppThemeLight")) {
            StylingVar = "<style>" + VideoStyling + ImageStyling + "div{max-width:100%; height: auto;}</style>";
        } else if (name.equals("AppThemeDark")) {

            StylingVar = "<style>" + VideoStyling + ImageStyling + " body{color: white; background: #262626;}  div{max-width:100%; height: auto;}</style>";
        }

        ArrayList<NewsItem> mList = ((globalClass) getApplication()).getNewsSourceByName(CurrentNewsSource.sourcename).getNewsItemArrayList();
        webViewLink = mList.get(currentIndexForNewsItem).contentEncoded;
        if (mList.get(currentIndexForNewsItem).contentEncoded == null) {

            webViewLink = mList.get(currentIndexForNewsItem).descriptionWithHtml;
        }
        HeadingText = mList.get(currentIndexForNewsItem).title;

        String tempTimeToPresent = mList.get(currentIndexForNewsItem).pubTime;
        NewsDetailText = mList.get(currentIndexForNewsItem).sourceName + "/ " + tempTimeToPresent.replace(toBeReplaced, toPutForReplacement);

        getSupportActionBar().setTitle(mList.get(currentIndexForNewsItem).sourceName);
        TemplateHtml = "<h1>" + HeadingText + "</h1> <div> <hr> " + NewsDetailText + " <hr></div>";
    }

    private int giveDpToPx(int dp) {
        DisplayMetrics displayMetrics = NewsDetailActivity.this.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    private void setCorrectTheme() {
        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
        }
        String name = ((globalClass) getApplication()).getApp_CurrentThemeName();
        if (name.equals("AppThemeNaija")) {
            setTheme(R.style.AppThemeNaija_NoActionBar);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
            }


            StylingVar = "<style> video{max-width:300; height: auto;} body{color: white; background: #262626;}   img{max-width:300; height: auto;} </style>";
        }

        if (name.equals("AppThemeLight")) {
            setTheme(R.style.AppThemeLight);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark_Light));
            }

            StylingVar = StylingConstant;
        }

        if (name.equals("AppThemeDark")) {
            setTheme(R.style.AppThemeDark);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
            }
            StylingVar = "<style> video{max-width:300; height: auto;} body{color: white; background: #262626;}   img{max-width:300; height: auto;} </style>";
        }
    }

}