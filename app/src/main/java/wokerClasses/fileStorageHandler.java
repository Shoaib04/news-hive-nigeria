package wokerClasses;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Shoaib on 5/31/2016.
 */
public class fileStorageHandler {

    private Context mContext;
    private FileOutputStream fos;
    private FileInputStream fis;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    public fileStorageHandler (Context context)
    {
        this.mContext = context;
    }

    public synchronized Object readObject (Class<?> theclass,String theFileName)
    {
        Object object=null;

        try {

            fis = mContext.openFileInput(theFileName);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            Log.e("fis","fis Exception Caught");

            return null;
        }

        try {
            ois = new ObjectInputStream(fis);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        try {
            object = ois.readObject();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fis.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ois.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return object;


//            try {
//                return theclass.newInstance();
//            }
//            catch (InstantiationException e)
//            {
//                e.printStackTrace();
//            } catch (IllegalAccessException e)
//            {
//                e.printStackTrace();
//            }
//            finally {
//                Log.d("instanceCreation","Instance not created for hashmap");
//                return null;
//            }
    }



    public synchronized void writeObject (String theFileName,Object object){

        try {
            fos = mContext.openFileOutput(theFileName,Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            oos = new ObjectOutputStream(fos);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("objectOutputStream","objectOutput Exception caught");
        }

        try {
            oos.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
