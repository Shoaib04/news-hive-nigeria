package wokerClasses;


import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import neededObjects.NewsItem;
import neededObjects.NewsSource;

/**
 * Created by Shoaib on 5/31/2016.
 */
public class feedParserClass {

    //We will be using no NameSpace for the feed XML file we receive
    private final String nameSpace = null;
    private final String TAG_RSS = "rss";
    private final String TAG_ITEM = "item";
    private final String TAG_ENTRY = "entry";
    private final String TAG_TITLE = "title";
    private final String TAG_LINK = "link";
    private final String TAG_DESCRIPTION = "description";
    private final String TAG_SUMMARY = "summary";
    private final String TAG_CONTENCODED = "content:encoded";
    private final String TAG_PUBDATE = "pubDate";
    private final String TAG_AUTHOR = "dc:creator";
    private final String TAG_ALTERAUTHOR = "author";
    private final String TAG_CHANNEL = "channel";
    private final String TAG_LASTBUILDDATE = "lastBuildDate";

    private NewsSource NewsSourceOBject_ParsingFor;
    private boolean shouldContinueParsing = true;
    private String dateBeforeUpgradationNewsSouce = null;
    boolean ifEncodeExists = false;
    SimpleDateFormat dateFormat;


//    public feedParserClass(String name) {
//        sourceName_ParsingFor = name;
//        dateFormat = new SimpleDateFormat("EEE, DD MMM yyyy HH:mm:ss");
//        ifEncodeExists = false;
//    }

    public feedParserClass(NewsSource gottenObject) {
        NewsSourceOBject_ParsingFor = gottenObject;
        NewsSourceOBject_ParsingFor.NewItemsGotten=0;
        dateBeforeUpgradationNewsSouce = NewsSourceOBject_ParsingFor.last_Updated;
        dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        ifEncodeExists = false;
    }

    public synchronized NewsSource ParseGivenFeed(InputStream inputStream) throws XmlPullParserException, IOException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false); //We don't need to handle namespaces
        parser.setInput(inputStream, null);
        parser.nextTag();
        return readFeed(parser);
    }

    private NewsSource readFeed(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, nameSpace, TAG_RSS);
        String titleForItem = null;
        String linkForItem = null;
        String descriptionForItem = null;
        String descriptionWithHtmlForItem_LatestAddition = null;
        String imageLinkForItem = null;
        String greaterDescForItem = null;
        String descriptionWithHtml = null;
        String pubDateForItem = null;
        String authorForItem = null;

        List<NewsItem> feedList = new ArrayList<NewsItem>();

        //while (true) {
        //    if (parser.getEventType() == XmlPullParser.START_TAG && (parser.getName().equals(TAG_CHANNEL) || parser.getName().equals(TAG_ENTRY)) || parser.getName().equals(TAG_ITEM)) {
        //        break;
        //    }
        //    parser.next();
        //}
        while (parser.next() != XmlPullParser.END_DOCUMENT)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG) {

                if (parser.getEventType() == XmlPullParser.END_TAG && (parser.getName().equals(TAG_ITEM) || parser.getName().equals(TAG_ENTRY))) {
                    if (titleForItem != null && linkForItem != null && descriptionForItem != null && authorForItem != null && (pubDateForItem != null || pubDateForItem.length() != 0)) {

                        NewsItem item = new NewsItem();
                        item.title = titleForItem;
                        item.link = linkForItem;
                        item.description = descriptionForItem;
                        item.descriptionWithHtml = descriptionWithHtmlForItem_LatestAddition;
                        item.sourceName = this.NewsSourceOBject_ParsingFor.sourcename;
                        item.imageLink = imageLinkForItem;
                        item.author = authorForItem;
                        item.pubTime = pubDateForItem;

                        if (ifEncodeExists) {
                            item.contentEncoded = greaterDescForItem;
                        } else {
                            item.contentEncoded = greaterDescForItem;
                        }

                        feedList.add(item);

                        titleForItem = null;
                        linkForItem = null;
                        descriptionForItem = null;
                        imageLinkForItem = null;
                        greaterDescForItem = null;
                        pubDateForItem = null;
                        authorForItem = null;

                    }
                }


                continue;
            }
            String name = parser.getName();
            if (name.equals(TAG_TITLE)) {
                titleForItem = readTitle(parser);
            } else if (name.equals(TAG_LINK)) {
                linkForItem = readLink(parser);
            } else if (name.equals(TAG_DESCRIPTION) || name.equals(TAG_SUMMARY)) {
                String[] arr = readDescriptionAndImageLink(parser);
                descriptionWithHtml = arr[0];
                if (arr[0] != null) {
                    descriptionForItem = avoidHtml(arr[0]);
                }
                if (arr[2] != null) {
                    descriptionWithHtmlForItem_LatestAddition = arr[2];
                }
                imageLinkForItem = arr[1];
            } else if (name.equals(TAG_CONTENCODED)) {

                ifEncodeExists = true;
                greaterDescForItem = readEncodedContent(parser);

            } else if (name.equals(TAG_PUBDATE)) {
                pubDateForItem = readPubDate(parser);
//                pubDateForItem = pubDateForItem.trim();
            } else if (name.equals(TAG_AUTHOR) || name.equals(TAG_ALTERAUTHOR)) {

                if (name.equals(TAG_AUTHOR)) {
                    authorForItem = readAuthor(parser, TAG_AUTHOR);
                    String aa = authorForItem;
                    authorForItem = aa;
                } else {
                    authorForItem = readAuthor(parser, TAG_ALTERAUTHOR);
                }

            } else if (name.equals(TAG_CHANNEL)) {

                checkChannelStatus(parser);

            }

            if (!shouldContinueParsing) {
                return NewsSourceOBject_ParsingFor;
            }

        }

        return giveNewsSourceObjectUpdateNewsList(feedList);
        // return feedList;
    }


    private NewsSource giveNewsSourceObjectUpdateNewsList(List<NewsItem> mList) {
        ArrayList<NewsItem> ListFromNewsSource = NewsSourceOBject_ParsingFor.getNewsItemArrayList();
        if (ListFromNewsSource == null) {
            ListFromNewsSource = new ArrayList<>();
        }
        if (ListFromNewsSource.size() == 0) {
            //When the list is previously Empty
        }

        //Remove the News From the Current List that Source holds, the News that are older than 2 days
        for (int i = 0; i < ListFromNewsSource.size(); i++) {
            if (NewsSourceOBject_ParsingFor.last_Updated != null) {
                try {
                    String dateFor_NewsSource_WasLast_Updated = NewsSourceOBject_ParsingFor.last_Updated;
                    String dateFor_NewsItem_FromCurrentIteration_Date = ListFromNewsSource.get(i).pubTime;

                    Date dateA = dateFormat.parse(dateFor_NewsSource_WasLast_Updated);
                    Date dateB = dateFormat.parse(dateFor_NewsItem_FromCurrentIteration_Date);

                    if (dateA.equals(dateB) || dateA.after(dateB)) {
                        if (dateA.after(dateB)) {
                            long elaspedHours = getDateDifference(dateB, dateA);
                            if (elaspedHours > 48) {
                                if (ListFromNewsSource.get(i).isRead == 1) {
                                    ListFromNewsSource.remove(i);
                                }
                            }
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.e("dateFormatError", "Inside giveNewsSourceObjectUpdateNewsList");
                }
            }
        }

        //Add the updated News that is the new news into the ListFromNewsSource ArrayList
        for (int i = 0; i < mList.size(); i++) {
            if (NewsSourceOBject_ParsingFor.last_Updated != null && dateBeforeUpgradationNewsSouce != null) {
                try {

                    String a = NewsSourceOBject_ParsingFor.last_Updated;
                    String b = mList.get(i).pubTime;
                    Date dateA = dateFormat.parse(NewsSourceOBject_ParsingFor.last_Updated);
                    Date dateB = dateFormat.parse(mList.get(i).pubTime);
                    Date dateC = dateFormat.parse(dateBeforeUpgradationNewsSouce);

                    if (dateA.equals(dateB) || dateA.before(dateB) || dateC.before(dateB)) {

                        //Add and also increment the var that shows Number of New items Added
                        NewsSourceOBject_ParsingFor.NewItemsGotten++;
                        ListFromNewsSource.add(mList.get(i));
                    }

                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                    Log.e("dateFormatError2", "Inside giveNewsSourceObjectUpdateNewsList");
                }


            }
            else if (dateBeforeUpgradationNewsSouce == null)
            {
                //Add and also increment the var that shows Number of New items Added
                NewsSourceOBject_ParsingFor.NewItemsGotten++;
                ListFromNewsSource.add(mList.get(i));
            }
        }

        NewsSourceOBject_ParsingFor.setNewsItemArrayList(ListFromNewsSource);
        return NewsSourceOBject_ParsingFor;
    }


    private long getDateDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return elapsedHours;
    }

    private void checkChannelStatus(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, nameSpace, TAG_CHANNEL);
        while (true) {
            if (parser.next() == XmlPullParser.START_TAG) {
                String name = parser.getName();
                if (name.equals(TAG_LASTBUILDDATE)) {
                    if (parser.next() == XmlPullParser.TEXT) {

                        if (NewsSourceOBject_ParsingFor.last_Updated != null) {
                            String newDateContainedTemp = parser.getText();
                            if (NewsSourceOBject_ParsingFor.last_Updated.equals(newDateContainedTemp)) {
                                dateBeforeUpgradationNewsSouce = NewsSourceOBject_ParsingFor.last_Updated;
                                shouldContinueParsing = false;
                                System.out.println("print dateBefore Upgradation " + dateBeforeUpgradationNewsSouce);

                            } else {
                                String mDateStringTemp = parser.getText();
                                NewsSourceOBject_ParsingFor.last_Updated = mDateStringTemp;
                                System.out.println("print dateUpdated LastOne " + mDateStringTemp);

                            }
                            return;
                        } else {
                            NewsSourceOBject_ParsingFor.last_Updated = parser.getText();
                            return;
                        }
                    }

                }
            } else if (parser.getEventType() == XmlPullParser.END_TAG) {
                if (parser.getName().equals(TAG_CHANNEL)) {
                    break;
                }

            }
        }

        //   parser.require(XmlPullParser.END_TAG, nameSpace, TAG_CHANNEL);

    }

    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, nameSpace, TAG_TITLE);
        String Title;
        Title = extractText(parser);
        parser.require(XmlPullParser.END_TAG, nameSpace, TAG_TITLE);
        return Title;

    }

    private String readEncodedContent(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, nameSpace, TAG_CONTENCODED);
        String desc;
        desc = extractTextForEncodedContent(parser);
        parser.require(XmlPullParser.END_TAG, nameSpace, TAG_CONTENCODED);
        return desc;
    }

    private String readPubDate(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, nameSpace, TAG_PUBDATE);
        String theDate;
        theDate = extractText(parser);
        try {
            Date tempDatefoNow = dateFormat.parse(theDate);
            theDate = dateFormat.format(tempDatefoNow);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        parser.require(XmlPullParser.END_TAG, nameSpace, TAG_PUBDATE);
        return theDate;

    }

    private String readAuthor(XmlPullParser parser, String GivenTag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, nameSpace, GivenTag);
        String theAuthor;
        theAuthor = extractText(parser);
        parser.require(XmlPullParser.END_TAG, nameSpace, GivenTag);
        return theAuthor;
    }

    private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, nameSpace, TAG_LINK);
        String Link;
        Link = extractText(parser);
        parser.require(XmlPullParser.END_TAG, nameSpace, TAG_LINK);
        return Link;

    }

    private String[] readDescriptionAndImageLink(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, nameSpace, TAG_DESCRIPTION);
        String[] arr = new String[3];
        if (parser.next() == XmlPullParser.TEXT) {
            String imgText = parser.getText();
            String descText = parser.getText();
            String DescriptionWithHtmlHere = parser.getText();
            imgText = getImageLink(imgText);
            // descText = avoidHtml(descText);
            parser.nextTag();

            if (imgText != null) {
                imgText = removeQuots(imgText);
            }
            arr[0] = descText;
            arr[1] = imgText;
            arr[2] = DescriptionWithHtmlHere;

        }

        parser.require(XmlPullParser.END_TAG, nameSpace, TAG_DESCRIPTION);
        return arr;
    }

    private String extractText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String Result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            Result = parser.getText();
            Result = avoidHtml(Result);
            parser.nextTag();
        }
        return Result;
    }

    private String extractTextForEncodedContent(XmlPullParser parser) throws IOException, XmlPullParserException {
        String Result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            Result = parser.getText();
            parser.nextTag();
        }
        return Result;
    }

    private String removeQuots(String text) {
        if (text.contains("\"")) {
            text = text.replace("\"", "");
            return text;
        }

        return text;
    }

    private String avoidHtml(String Result) {
        if (Result.contains("\n")) {
            Result = Result.replace("\n", "");
        }

        return removeImageSpanObjects(Result).toString();
        //return Html.fromHtml(Result).toString();
    }

    public static Spanned removeImageSpanObjects(String inStr) {
        SpannableStringBuilder spannedStr = (SpannableStringBuilder) Html
                .fromHtml(inStr.trim());
        Object[] spannedObjects = spannedStr.getSpans(0, spannedStr.length(),
                Object.class);
        for (int i = 0; i < spannedObjects.length; i++) {
            if (spannedObjects[i] instanceof ImageSpan) {
                ImageSpan imageSpan = (ImageSpan) spannedObjects[i];
                spannedStr.replace(spannedStr.getSpanStart(imageSpan),
                        spannedStr.getSpanEnd(imageSpan), "");
            }
        }
        return spannedStr;
    }

    private String getImageLink(String givenString) {
        Log.e("Parse tell", NewsSourceOBject_ParsingFor.sourcename);
        if (givenString.contains("<img")) {
            String trimmedString = givenString.substring(0, givenString.indexOf("<img"));
            givenString = givenString.replace(trimmedString, "");

            String result;
            if (givenString.contains("jpg\"")) {
                result = givenString.substring(givenString.indexOf("src=\"") + 5, givenString.indexOf("jpg\"") + 4);
                return result;
            } else if (givenString.contains("jpeg\"")) {
                result = givenString.substring(givenString.indexOf("src=\"") + 5, givenString.indexOf("jpeg\"") + 5);
                return result;
            }

            return null;


        } else {
            return null;
        }

    }
}
