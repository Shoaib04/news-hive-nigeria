package neededObjects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by Shoaib on 5/30/2016.
 */
public class NewsItem implements Serializable, Parcelable, Comparable{

    public String title=null;
    public String link=null;
    public String description=null;
    public String descriptionWithHtml=null;
    public String sourceName=null;
    public String imageLink=null;
    public Byte Shown=0;
    public String contentEncoded=null;
    public String pubTime=null;
    public String author=null;
    public Byte isRead=0;
    public Byte isFav=0;


    public NewsItem ()
    {


    }

//    public String getPubTime () {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, DD MMM yyyy HH:mm:ss");
//        if (pubTime!=null)
//        {
//            try {
//                Date tempDate =dateFormat.parse(pubTime);
//                pubTime = dateFormat.format(tempDate);
//
//                return pubTime;
//            } catch (ParseException e) {
//                e.printStackTrace();
//
//            }
//        }
//        else
//        {
//            return "0 hours";
//        }
//        return "0 hours";
//    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(link);
        dest.writeString(description);
        dest.writeString(descriptionWithHtml);
        dest.writeString(sourceName);
        dest.writeString(imageLink);
        dest.writeByte(Shown);
        dest.writeString(contentEncoded);
        dest.writeString(pubTime);
        dest.writeByte(isRead);
        dest.writeByte(isFav);

    }

    public static final Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
        @Override
        public NewsItem createFromParcel(Parcel in) {
            return new NewsItem(in);
        }

        @Override
        public NewsItem[] newArray(int size) {
            return new NewsItem[size];
        }
    };

    private NewsItem (Parcel parcel)
    {
        title = parcel.readString();
        link = parcel.readString();
        description = parcel.readString();
        descriptionWithHtml = parcel.readString();
        sourceName = parcel.readString();
        imageLink = parcel.readString();
        Shown = parcel.readByte();
        contentEncoded = parcel.readString();
        pubTime = parcel.readString();
        isRead = parcel.readByte();
        isFav = parcel.readByte();
    }

    private long getDateTime (String givenPubTime)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            return dateFormat.parse(givenPubTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public int compareTo(Object another) {
        NewsItem givenItem = (NewsItem) another;
        long givenObjectsDate = getDateTime(givenItem.pubTime);
        long thisObjectsDate = getDateTime(this.pubTime);

        //Condition for Recent to Old News, That is Descending.
        long theTimeDiff = givenObjectsDate - thisObjectsDate;
        return (int)theTimeDiff;
    }
}
